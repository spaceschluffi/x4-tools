import csv, collections, logging

TRANSLATIONS = {
    "Y": "systemnames",
    "C": "sectornames",
    "F": "factionnames",
    "S": "shipnames",
}
TEMPLATE = "X4_map_131_template"
LOGLEVEL = logging.DEBUG


def read_csv(fd):
    ret = collections.defaultdict(dict)
    reader = csv.reader(fd)
    langs = next(reader)[1:]
    for line in reader:
        lineid = line.pop(0).strip()
        for lang, t in zip(langs, line):
            ret[lang.strip()][lineid] = t.strip()
    return ret


def read_translations():
    ret = {}
    for code, filename in TRANSLATIONS.items():
        logging.debug("Reading file " + filename + " for code " + code)
        with open(filename + ".csv") as f:
            ret[code] = read_csv(f)
    return ret


if __name__ == "__main__":
    logging.basicConfig(level=LOGLEVEL)
    trans = read_translations()
    with open(TEMPLATE + ".svg") as template:
        langs = sorted(trans["F"].keys())
        for lang in langs:
            logging.info("Handling language " + lang)
            with open(TEMPLATE + "_" + lang + ".svg", "w") as outfile:
                template.seek(0)
                while True:
                    # read the template file in chunks
                    buf = template.read(4096)
                    if not buf:
                        break
                    while True:
                        # search for $ templates and replace them
                        # (this would be easier with regex)
                        pos = buf.find("$")
                        if pos == -1:
                            break
                        outfile.write(buf[:pos])
                        buf = buf[pos:]
                        # now buf[0] is the "$", buf[1] is the code, and the
                        # next number digits tell us the translation id
                        if len(buf) < 1024:
                            # a template may sit on a chunk border, but we can
                            # load more data if the input file is not at EOF
                            buf += template.read(4096) or ""
                        code = buf[1]
                        idx = 2
                        tid = ""
                        while buf[idx] in "0123456789":
                            tid += buf[idx]
                            idx += 1
                        t = trans[code][lang][tid]
                        outfile.write(t)
                        buf = buf[idx:]
                    outfile.write(buf)
