A collection of tools for X4 Foundations by Egosoft
===================================================

So far includes a **translation.py** script which collects translation strings
from the game's XML files and builds csv tables, as well as **templating.py**
which uses those files to generate a map for all languages from an SVG template.

