PATH_GAME = "~/opt/Games/X4 Foundations/game"
PATH_LANGS = "unpacked/09/t"
REFERENCE = r"\{(\d+)?,(\d+)\}"
IDS_LANGS = {
    "de": 49,
    "en": 44,
    "ru": 7,
    "fr": 33,
    "es": 34,
    "it": 39,
    "pt": 55,
    "jp": 81,
    "kr": 82,
    "cn": 86,
    "tw": 88,
}


import xml.etree.ElementTree as ET, os.path, logging, re, itertools, io

logging.basicConfig(level=logging.INFO)

REFERENCE_RE = re.compile(REFERENCE)
PATH_GAME = os.path.expanduser(PATH_GAME)

logging.info("Parsing language files:")
xmltrees = {}

for lang, code in IDS_LANGS.items():
    filename = "0001-l{:03d}.xml".format(code)
    logging.info("  {} - {}".format(lang, filename))
    xmltrees[lang] = ET.parse(os.path.join(PATH_GAME, PATH_LANGS, filename))


def translate(etree, page, t):
    """Find translation string for page and t ids"""
    logging.debug("Translating page {} and t {}".format(page, t))
    ret = etree.find("page[@id='{}']/t[@id='{}']".format(page, t))
    if ret is not None:
        ret = ret.text
        logging.debug("  Result: " + repr(ret))
        for match in REFERENCE_RE.finditer(ret):
            logging.debug("    Resolving submatch: " + repr(match.groups()))
            page, t = match.groups(page)
            subst = translate(etree, page, t)
            logging.debug(
                "    Resolved submatch: " + repr(match.groups()) + " to " + repr(subst)
            )
            if subst:
                ret = ret.replace(match[0], subst)
                # ret = ret[:match.start()] + subst + ret[match.end():]
    else:
        logging.warning("Didn't find reference: {{{},{}}}".format(page, t))
    return ret


def remove_braces(s):
    """Remove (...) from string, convert None to "-"."""
    if s is None:
        return "-"
    count = 0
    buf = io.StringIO()
    for c in s:
        if c == "\\" and not quote:
            quote = True
        else:
            quote = False
        if c == "(" and not quote:
            count += 1
        elif c == ")" and not quote:
            count = max(0, count - 1)
        else:
            if count == 0:
                buf.write(c)
    return buf.getvalue()


def usage():
    print(
        """Call this program with a single word as first argument telling it what to do:
systemnames - write a systemnames.csv with translations of all known systems
shipnames - same for ships
factionnames - same for factions
test - access some translation strings to test basic operation"""
    )


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 2:
        usage()
        sys.exit(1)
    action = sys.argv[1]
    if action == "systemnames":
        outf = open("systemnames.csv", "w")
        outf.write("system id, " + ", ".join(sorted(IDS_LANGS.keys())) + "\n")
        for i in itertools.chain(range(1, 50 + 1), range(400, 425 + 1)):
            outf.write(str(i) + ", ")
            for lang in sorted(IDS_LANGS.keys()):
                outf.write(
                    remove_braces(translate(xmltrees[lang], "20003", str(i) + "0001"))
                    + ", "
                )
            outf.write("\n")
    elif action == "sectornames":
        # page 20004
        outf = open("sectornames.csv", "w")
        outf.write("sector id, " + ", ".join(sorted(IDS_LANGS.keys())) + "\n")
        for sysi in itertools.chain(range(1, 50 + 1), range(400, 425 + 1)):
            # walk over system ids and test for sectors
            seci = 1
            while True:
                fullid = str(sysi) + "00" + str(seci)
                trans = translate(xmltrees["en"], "20004", fullid + "1")
                if trans is None:
                    break
                outf.write(fullid + ", ")
                for lang in sorted(IDS_LANGS.keys()):
                    outf.write(
                        remove_braces(translate(xmltrees[lang], "20004", fullid + "1"))
                        + ", "
                    )
                outf.write("\n")
                seci += 1
    elif action == "shipnames":
        # ships: page 20101
        # Argon 101-137(01), Teladi 201-229, Paranid 301-330, Split 401-432, Boron 501-530, Terran 601-630, Xenon 701-706 and unused 708-715, Pirate 801-810, Kha'ak 901-906, Misc 1201-1209
        outf = open("shipnames.csv", "w")
        outf.write("ship id, " + ", ".join(sorted(IDS_LANGS.keys())) + "\n")
        for i in itertools.chain(
            range(101, 137 + 1),
            range(201, 229 + 1),
            range(301, 330 + 1),
            range(401, 432 + 1),
            range(501, 530 + 1),
            range(601, 630 + 1),
            range(701, 706 + 1),
            range(801, 810 + 1),
            range(901, 906 + 1),
            range(1201, 1209 + 1),
        ):
            outf.write(str(i) + ", ")
            for lang in sorted(IDS_LANGS.keys()):
                outf.write(
                    remove_braces(translate(xmltrees[lang], "20101", str(i) + "01"))
                    + ", "
                )
            outf.write("\n")
    elif action == "factionnames":
        outf = open("factionnames.csv", "w")
        outf.write("faction id, " + ", ".join(sorted(IDS_LANGS.keys())) + "\n")
        for i in range(1, 26 + 1):
            outf.write(str(i) + ", ")
            for lang in sorted(IDS_LANGS.keys()):
                outf.write(
                    remove_braces(translate(xmltrees[lang], "20203", str(i) + "01"))
                    + ", "
                )
            outf.write("\n")
    elif action == "test":
        tree = xmltrees["it"]
        name = translate(tree, "20004", "40011")
        print(name)  # Nopileus Fortune II
        desc = translate(tree, "20003", "40002")
        print(desc)  # and desc
        desc = translate(tree, "20003", "150002")  # Ianamus Zura desc
        print(desc)
        print(translate(tree, "10099", "1023"))  # Attention all pilots may...
    else:
        usage()
        sys.exit(1)
